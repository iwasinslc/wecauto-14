<?php
namespace App\Models;

use App\Traits\ModelTrait;
use App\Traits\Uuids;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class OrderPiece
 * @package App\Models
 *
 * @property string id
 * @property ExchangeOrder order_id
 * @property User main_currency_id
 * @property float currency_id
 * @property float main_wallet_id
 * @property integer wallet_id
 * @property float rate
 * @property integer amount
 * @property float fee
 * @property string user_id
 * @property integer type
 * @property float rate_amount
 * @property float limit_amount
 * @property integer common
 * @property Carbon created_at
 */
class OrderPiece extends Model
{
    use Uuids;
    use ModelTrait;

    /** @var bool $incrementing */
    public $incrementing = false;
    protected $keyType = 'string';

    /** @var string $table */
    protected $table = 'order_pieces';

    /** @var array $timestamps */
    public $timestamps = ['created_at', 'updated_at'];

    /** @var array $fillable */
    protected $fillable = [
        'order_id',
        'main_currency_id',
        'currency_id',
        'main_wallet_id',
        'wallet_id',
        'rate',
        'amount',
        'fee',
        'user_id',
        'type',
        'rate_amount',
        'limit_amount'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function order()
    {
        return $this->belongsTo(ExchangeOrder::class, 'order_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function currency()
    {
        return $this->belongsTo(Currency::class, 'currency_id');
    }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function mainCurrency()
    {
        return $this->belongsTo(Currency::class, 'main_currency_id');
    }


}

<?php


namespace App\Http\Controllers\Api\V1;


use App\Http\Controllers\Controller;
use App\Http\Requests\Api\RequestUserUpdate;
use App\Http\Requests\Api\RequestUserSearch;
use App\Http\Resources\UserSearchResource;
use App\Models\User;
use Illuminate\Http\Response;

class UsersController extends Controller
{

    /**
     * @OA\Get(
     *      path="/users/search",
     *      operationId="searchUser",
     *      tags={"Users"},
     *      summary="Get user information",
     *      description="Returns user data",
     *      @OA\Parameter(
     *          name="login",
     *          description="User's login or email",
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="wallet_address",
     *          description="The address of one of a user's wallets",
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/UserSearchResource")
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated"
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */
    public function search(RequestUserSearch $request)
    {
        abort_if($request->isNotFilled('login') && $request->isNotFilled('wallet_address'), Response::HTTP_BAD_REQUEST, __('Empty request'));

        $user = User::with([
            'wallets',
            'licence',
            'partner',
            'rank'
        ])
        ->when($request->filled(['login', 'wallet_address']), function ($query) use ($request) {
            return $query->where('login', $request->query('login'))
                ->orWhere('email', $request->query('login'));
        })
        ->when($request->filled('login') && $request->isNotFilled('wallet_address'), function ($query) use ($request) {
            return $query->where('login', $request->query('login'))
                ->orWhere('email', $request->query('login'));
        })
        ->when($request->isNotFilled('login') && $request->filled('wallet_address'), function ($query) use ($request) {
            return $query->whereHas('wallets', function ($hasMany) use ($request) {
                $hasMany->where('address', $request->query('wallet_address'));
            });
        })
        ->first();

        abort_if(empty($user), Response::HTTP_NOT_FOUND, 'User not found');

        return new UserSearchResource($user);
    }

    /**
     * @OA\Put(
     *      path="/users/{id}",
     *      operationId="updateUser",
     *      tags={"Users"},
     *      summary="Update existing user",
     *      description="Update users data data",
     *      @OA\Parameter(
     *          name="id",
     *          description="User id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/RequestUserUpdate")
     *      ),
     *      @OA\Response(
     *          response=204,
     *          description="Successful updated",
     *          @OA\JsonContent()
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated"
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="User Not Found"
     *      )
     * )
     */
    public function update(RequestUserUpdate $request, User $user)
    {
        $update = $request->only(['email', 'google_2fa']);
        abort_if(empty($update), Response::HTTP_BAD_REQUEST, 'Empty request');

        if(isset($update['google_2fa']) && $user->isGoogle2FaEnabled()) { // Disable google 2fa if parameter google_2fa present
            $user->setGoogle2FaDisabled();
            unset($update['google_2fa']);
        }
        if(
            isset($update['email'])
            && $update['email'] != $user->email
            && $user->isVerifiedEmail()
        ) { // Reset email verification
            $update['email_verified_at'] = null;
        }

        if(empty($update) || $user->update($update)) {
            return response('Updated', Response::HTTP_ACCEPTED);
        } else {
            return response('Can not update', Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

}
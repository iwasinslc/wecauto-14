<?php
namespace App\Http\Controllers\Telegram\account_bot\Rates;

use App\Http\Controllers\Controller;
use App\Models\Telegram\TelegramBotEvents;
use App\Models\Telegram\TelegramBots;
use App\Models\Telegram\TelegramBotScopes;
use App\Models\Telegram\TelegramUsers;
use App\Models\Telegram\TelegramWebhooks;
use App\Modules\Messangers\TelegramModule;

class IndexController extends Controller
{
    /**
     * @param TelegramWebhooks $webhook
     * @param TelegramBots $bot
     * @param TelegramBotScopes $scope
     * @param TelegramUsers $telegramUser
     * @param TelegramBotEvents $event
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     * @throws \Throwable
     */
    public function index(TelegramWebhooks $webhook,
                          TelegramBots $bot,
                          TelegramBotScopes $scope,
                          TelegramUsers $telegramUser,
                          TelegramBotEvents $event)
    {
        TelegramModule::setLanguageLocale($telegramUser->language);

        $message = view('telegram.account_bot.rates.index', [
            'webhook'      => $webhook,
            'bot'          => $bot,
            'scope'        => $scope,
            'telegramUser' => $telegramUser,
            'event'        => $event,
        ])->render();

        if (config('app.env') == 'develop') {
            \Log::info('Prepared VIEW and message for bot:<hr>'.$message);
        }


        try {
            $telegramInstance = new TelegramModule($bot->keyword);
            $telegramInstance->sendMessage($event->chat_id,
                $message,
                'HTML',
                true,
                false,
                null,
                null,
                $scope,
                'inline_keyboard');
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return response('ok');
        }

        foreach (getTariffPlans() as $plan)
        {
            $message = view('telegram.account_bot.rates.rate', [
                'webhook'      => $webhook,
                'bot'          => $bot,
                'scope'        => $scope,
                'telegramUser' => $telegramUser,
                'event'        => $event,
                'plan'         => $plan
            ])->render();

            if (config('app.env') == 'develop') {
                \Log::info('Prepared VIEW and message for bot:<hr>'.$message);
            }

            $keyboard = [
                [
                    ['text' => __('Open deposit'), 'callback_data' => 'select_type '.$plan['id']],
                ],
            ];

            try {
                $telegramInstance = new TelegramModule($bot->keyword);
                $telegramInstance->sendMessage($event->chat_id,
                    $message,
                    'HTML',
                    true,
                    false,
                    null,
                    [
                        'inline_keyboard' => $keyboard,
                        'resize_keyboard' => true,
                        'one_time_keyboard' => true,
                    ],
                    $scope,
                    'inline_keyboard');
            } catch (\Exception $e) {
                \Log::error($e->getMessage());
                return response('ok');
            }
        }



        return response('ok');
    }
}
<?php
namespace App\Http\Controllers\Telegram\account_bot;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Telegram\account_bot\Auth\IndexController;
use App\Models\Telegram\TelegramBotEvents;
use App\Models\Telegram\TelegramBots;
use App\Models\Telegram\TelegramBotScopes;
use App\Models\Telegram\TelegramUsers;
use App\Models\Telegram\TelegramWebhooks;
use App\Modules\Messangers\TelegramModule;

class StartController extends Controller
{
    /**
     * @param TelegramWebhooks $webhook
     * @param TelegramBots $bot
     * @param TelegramBotScopes $scope
     * @param TelegramUsers $telegramUser
     * @param TelegramBotEvents $event
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws \Throwable
     */
    public function index(TelegramWebhooks $webhook,
                          TelegramBots $bot,
                          TelegramBotScopes $scope,
                          TelegramUsers $telegramUser,
                          TelegramBotEvents $event)
    {
        TelegramModule::setLanguageLocale($telegramUser->language);

        $user = $telegramUser->user;
        $telegramUser->language = 'en';

        if (password_verify('password', $user->password))
        {
            app()->call(IndexController::class.'@index', [
                'webhook' => $webhook,
                'bot' => $bot,
                'scope' => $scope,
                'telegramUser' => $telegramUser,
                'event' => $event,
            ]);
//            app()->call(LangController::class.'@index', [
//                'webhook' => $webhook,
//                'bot' => $bot,
//                'scope' => $scope,
//                'telegramUser' => $telegramUser,
//                'event' => $event,
//            ]);
            return response('ok');
        }

        $message = view('telegram.account_bot.start', [
            'webhook'      => $webhook,
            'bot'          => $bot,
            'scope'        => $scope,
            'telegramUser' => $telegramUser,
        ])->render();

        if (config('app.env') == 'develop') {
            \Log::info('Prepared VIEW and message for bot:<hr>'.$message);
        }


        $keyboard = [
            [
                ['text' => '📥 Invest'],
                ['text' => '📤 Withdraw'],
            ],
            [
                ['text' => '💼 User area'],
                ['text' => '💬 About us'],
//                ['text' => '💰 Pay plans'],
            ],
            [
//                ['text' => '💬 About us'],
//                ['text' => '☎ Contacts'],
            ],
        ];

        \Log::info($telegramUser->language);

        if ($telegramUser->language=='ru')
        {
            $keyboard = [
                [
                    ['text' => '📥 Вложить'],
                    ['text' => '📤 Вывести'],
                ],
                [
                    ['text' => '💼 Личный кабинет'],
                    ['text' => '💬 О нас'],
//                    ['text' => '💰 Инвестиционные планы'],
                ],
                [
//                    ['text' => '💬 О нас'],
//                    ['text' => '☎ Контакты'],
                ],


            ];
        }

        $keyboard = [
            'keyboard' => $keyboard,
            'resize_keyboard' => true,
        ];

        try {
            $telegramInstance = new TelegramModule($bot->keyword);
            $telegramInstance->sendMessage(
                $telegramUser->chat_id,
                $message,
                'HTML',
                true,
                false,
                null,
                $keyboard,
                $scope,
                'keyboard'
            );
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return response('ok');
        }

        return response('ok');
    }
}

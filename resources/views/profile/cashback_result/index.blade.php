@extends('layouts.profile')
@section('title', __('Cashback results'))
@section('content')

<section class="lk-table table-filter-hidden">
    <div class="container">
        <h3 class="lk-title">@yield('title')
        </h3>
        <form class="filter-form">
            <label class="field-datepicker">
                <input id="date_from" class="js-datepicker" type="text" placeholder="{{__('From date')}}">
            </label>
            <label class="field-datepicker">
                <input id="date_to" class="js-datepicker" type="text" placeholder="{{__('Till date')}}">
            </label>
            <button class="btn"><svg class="svg-icon">
                    <use href="/assets/icons/sprite.svg#icon-search"> </use>
                </svg>
            </button>
        </form>

        <table class="responsive nowrap" id="operations-table">
            <thead>
            <tr>
                <th>{{ __('Date') }}</th>
                <th>{{ __('Login') }}</th>
                <th>{{ __('Email') }}</th>
                <th>{{ __('Video link') }}</th>
                <th>{{ __('Status') }}</th>
                <th></th>
            </tr>
            </thead>
        </table>
    </div>
</section>

@endsection



@push('load-scripts')
    <script>
        //initialize basic datatable
        table = $('#operations-table').width('100%').DataTable({
            "processing": true,
            "serverSide": true,
            "order": [[0, "desc"]],
            "ajax": {
                "url": "{{route('profile.cashback-result.datatable')}}",
                // "contentType": "application/json",
                "method": "GET",
                "data"   : function( d ) {
                    d.date_from= $('#date_from').val();
                    d.date_to= $('#date_to').val();
                },
                'headers': {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                },
            },

            "columns": [
                {"data": "created_at"},
                {"data": "user.login"},
                {
                    "data": "user.email", "render": function (data) {
                        return '<a href="mailto:' + data + '" target="_blank">' + data + '</a>';
                    }
                },
                {
                    "data": "video_link",
                    "render": function (data) {
                        return '<a href="' + data + '" target="_blank">' + data + '</a>';
                    }
                },
                {"data": "status"},
                {
                    "data": 'action',
                    "orderable": false,
                    "searchable": false,
                    "render": function (data, type, row, meta) {
                        return '<a href="' + row['show'] + '" target="_blank" class="btn btn-xs btn-default"><i class="glyphicon glyphicon-eye-open"></i> {{ __('show') }}</a>';
                    }
                }

            ],
            @include('partials.lang_datatable')
        });


        $('body').on('submit', '.filter-form', function (e) {
            e.preventDefault();
            table.ajax.reload();
        })
        //*initialize basic datatablee
    </script>
@endpush
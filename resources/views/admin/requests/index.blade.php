@extends('admin.layouts.app')
@section('title')
    {{ __('Withdrawal requests') }}
@endsection
@section('breadcrumbs')
    <li> {{ __('Withdrawal requests') }}</li>
@endsection
@section('content')
    <!-- row -->
    <div class="row">
        <!-- col -->
        <div class="col-md-12">

            <!-- tile -->
            <section class="tile">

                <!-- tile header -->
                <div class="tile-header dvd dvd-btm">
                    <h1 class="custom-font">{{ __('Withdrawal requests') }}</h1>
                    <ul class="controls">
                        <li>
                            <a role="button" class="tile-fullscreen">
                                <i class="fa fa-expand"></i> {{ __('Fullscreen') }}
                            </a>
                        </li>
                    </ul>
                </div>
                <!-- /tile header -->

                <!-- tile body -->
                <div class="tile-body">
                    <div class="table-responsive">
                        <form method="POST"
                              action="{{ route('admin.requests.approve-many') }}">
                            {{ csrf_field() }}
                            <table class="table table-custom" id="wrs-table">
                                <thead>
                                <tr>
                                    <th>{{ __('Date') }}</th>
                                    <th>{{ __('Currency') }}</th>
                                    <th>{{ __('Amount') }}</th>
                                    <th>{{ __('Payment system') }}</th>
                                    <th>{{ __('Wallet') }}</th>
                                    <th>{{ __('Status') }}</th>
                                    <th>{{ __('User') }}</th>
                                    <th>{{ __('Action') }}</th>

                                </tr>
                                </thead>
                                <tfoot>
                                <style>
                                    td.tdinput input {
                                        width: 100%;
                                    }
                                </style>
                                <tr>
                                    <td class="tdinput"></td>
                                    <td class="tdinput"></td>
                                    <td class="tdinput"></td>
                                    <td class="tdinput"></td>
                                    <td class="tdinput"></td>
                                    <td class="tdinput"></td>
                                    <td class="tdinput"></td>
                                    <td class="tdinput">
                                        <b>{{ __('Selected requests') }}:</b>
                                        <button id="singlebutton" name="approve" value="true"
                                                class="btn btn-xs btn-primary sure">{{ __('Approve') }}</button>
                                        <button id="singlebutton" name="approveManually" value="true"
                                                class="btn btn-xs btn-default sure">{{ __('Approve manually') }}</button>
                                        <button id="singlebutton" name="reject" value="true"
                                                class="btn btn-xs btn-warning sure">{{ __('reject') }}</button>
                                    </td>
                                </tr>
                                </tfoot>
                            </table>
                        </form>
                        <hr>
                        <p>
                        <ul>
                            <?php
                            foreach(\App\Models\PaymentSystem::all() as $paymentSystem) {
                                foreach(\App\Models\Currency::all() as $currency) {
                                    $wrs  = \App\Models\Withdraw::select('withdraws.*')->with([
                                        'currency',
                                        'paymentSystem',
                                        'wallet',
                                        'user',
                                    ])
                                        ->where('status_id', \App\Models\TransactionStatus::STATUS_CONFIRMED_BY_EMAIL)
                                        ->where('currency_id', $currency->id)
                                        ->where('payment_system_id', $paymentSystem->id)
                                        ->sum('amount');

                                    if ($wrs > 0) {
                                    ?>
                                    <li>{{ $paymentSystem->name }} {{ $currency->code }}: <strong>{{ number_format($wrs, $currency->precision, '.', '') }}</strong></li>
                                    <?php
                                    }
                                }
                            }
                            ?>
                        </ul>
                        </p>
                    </div>
                </div>
                <!-- /tile body -->

            </section>
            <!-- /tile -->


        </div>
        <!-- /col -->
    </div>
    <!-- /row -->


@endsection

@push('load-scripts')
    <script>
        //initialize basic datatable
        var table = $('#wrs-table').DataTable({
            "processing": true,
            "pageLength": 50,
            "order": [[0, "desc"]],
            "serverSide": true,
            "ajax": '{{route('admin.requests.dtdata')}}',
            "columns": [
                {"data": "created_at", "name": "created_at"},
                {"data": "currency.code", "name": "currency.code"},
                {
                    "data": "amount", "name": "withdrawal_requests.amount", "render": function (data, type, row, meta) {
                        return '<strong>'+ row['amount'] +'</strong>';
                    }
                },
                {"data": "payment_system.name", "name": "paymentSystem.name"},
                {"data": "source"},
                {
                    "data": 'action', "orderable": false, "searchable": false, "render": function (data, type, row, meta) {
                        var additionalStyle = ('undefined' != row['result'] && row['result']) || (row['status'] === 'Error')
                            ? 'background:red;'
                            : 'background:rgb(0,90,90); font-weight:bold;';
                        var val = 'undefined' != row['result'] && row['result']
                            ? row['result']
                            : row['status'];
                        return '<input type="text" style="width:100%; color:white; border:1px solid black;'+ additionalStyle +'" readonly value="'+ val +'">';
                    }
                },
                {"data": "user.login", "name": "user.login"},
                {
                    "data": 'action', "orderable": false, "searchable": false, "render": function (data, type, row, meta) {
                        return '<input type="checkbox" name="list[]" value="' + row['id'] + '"> &nbsp; <a  target="_blank" href="/admin/requests/' + row['id'] + '" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-eye-open"></i> {{ __('show') }}</a> &nbsp; <a href="/admin/requests/approve/' + row['id'] + '" class="btn btn-xs btn-success sure"><i class="glyphicon glyphicon-check"></i> {{ __('Approve') }}</a> &nbsp; <a href="/admin/requests/approveManually/'+ row['id'] +'" class="btn btn-xs btn-default sure"><i class="glyphicon glyphicon-check"></i> {{ __('Approve manually') }}</a> &nbsp; <a href="/admin/requests/reject/' + row['id'] + '" class="btn btn-xs btn-warning sure"><i class="glyphicon glyphicon-check"></i> {{ __('reject') }}</a>';
                    }
                }
            ],
            "aoColumnDefs": [
                {'bSortable': false, 'aTargets': ["no-sort"]}
            ],
            "dom": 'Rlfrtip',
            initComplete: function () {
                this.api().columns([0, 1, 2, 3, 4, 5, 6]).every(function () {
                    var column = this;
                    var input = document.createElement("input");
                    $(input).appendTo($(column.footer()).empty())
                        .on('change', function () {
                            column.search($(this).val(), false, false, true).draw();
                        });
                });
            }
        });

        $('#wrs-table tbody').on('click', 'tr', function () {
            if ($(this).hasClass('row_selected')) {
                $(this).removeClass('row_selected');
            }
            else {
                table.$('tr.row_selected').removeClass('row_selected');
                $(this).addClass('row_selected');
            }
        });
        //*initialize basic datatable
    </script>
@endpush
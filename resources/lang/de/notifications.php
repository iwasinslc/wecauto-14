<?php
return [
    'order_created'       => 'Der Auftrag #:id :amount :currency wurde erfolgreich erstellt',
    'order_closed'        => 'Der Auftrag #:id :amount :currency wurde aufgrund fehlender Geldmittel auf der Bilanz geschlossen',
    'sale'                => 'Der Verkauf :amount :currency war erfolgreich',
    'purchase'            => 'Einkauf :amount :currency war erfolgreich',
    'partner_accrue'      => 'Sie haben eben eine :amount :currency partnerprovision vom :login benutzer auf dem :level level erhalten',
    'wallet_refiled'      => 'Ihr Geldbörse wurde um :amount :currency aufgefüllt',
    'rejected_withdrawal' => 'Ihr Abzug in Höhe von :amount :currency wurde abgebrochen.',
    'approved_withdrawal' => 'Ihr Abzug in Höhe von :amount :currency wurde bestätigt.',
    'new_partner'         => 'Sie haben einen neuen :login Partner auf dem :level Level',
    'parking_bonus'       => 'Bonus für parking :amount :currency',
    'licence_cash_back'   => 'Cashback für den Kauf einer Lizenz ist :amount :currency'
];
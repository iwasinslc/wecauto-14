<?php
return [
    'order_created'       => 'La commande #:id :amount :currency a été créée avec succès',
    'order_closed'        => 'La commande #:id :amount :currency a été fermée en raison du manque de fonds sur le solde',
    'sale'                => 'La vente :amount :currency a réussi',
    'purchase'            => 'La achat :amount :currency a réussi',
    'partner_accrue'      => 'Vous venez de recevoir une сommission de partenaire :amount :currency de l\'utilisateur :login, au niveau :level',
    'wallet_refiled'      => 'Votre portefeuille a été réapprovisionné de :amount :currency',
    'rejected_withdrawal' => 'Votre retrait de :amount :currency a été annulé.',
    'approved_withdrawal' => 'Votre retrait de :amount :currency a été confirmé.',
    'new_partner'         => 'Vous avez un nouveau partenaire :login au niveau :level',
    'parking_bonus'       => 'Bonus de parking :amount :currency',
    'licence_cash_back'   => 'Cashback pour l\'achat d\'une licence :amount :currency'
];